# TeleTriage

TeleTriage es una aplicación web, que le facilita al personal médico la 
identificación de los síntomas de personas potencialmente contagiadas con el 
COVID-19. Esto surge como una iniciativa ciudadana y de trabajo voluntario, 
para afrontar emergencia.

La herramienta está desarrollada utilizando las siguientes tecnologías:

1. [Maria DB](https://mariadb.org/)
2. [PHP](https://www.php.net/manual/es/intro-whatis.php)
3. [Laravel](https://laravel.com/)


---

## Dependencias

1. [Composer](https://getcomposer.org/)
2. php-mbstring
3. php-dom

## Manejo de entornos

#### Local - desarrollo

1. Se debe copiar el archivo .env.local y renombrarlo a .env

2. Se debe instalar el proyecto
    ```
    composer install
    ```

3. editar .env para agregar los datos de la base

4. Generar la llave del proyecto
    ```
    php artisan key:generate
    ```

5. generar las llaves en el storage
    ```
    php artisan passport:install 
    ```

6. Configurar la base de datos

## Docker commands

**Run Local**
#### Build: El siguente comando se ejecuta una vez para generar la imagen APP_ENVIRONMENT (local, dev, prod)
```
docker build . -t teletriage-api-local  --build-arg APP_ENVIRONMENT=local -f Dockerfile
```
#### RUN: Para ejecutar en entorno local inicializando desde volumen - se debe correr previamente composer install en la raiz del proyecto
```
docker run --name=teletriage-api-local --env APP_ENVIRONMENT=local --rm -v $PWD:/var/www/html/symfony/ -p 80:80/tcp teletriage-api-local
```

#### RUN: Para ejecutar aislado
```
docker run --name=teletriage-api-local --env APP_ENVIRONMENT=local -p 80:80/tcp teletriage-api-local
```
