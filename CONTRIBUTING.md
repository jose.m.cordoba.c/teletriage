# Guia Para Contribuir en el Proyecto

## Para contribuir en el proyecto se deben seguir el siguiente procedimiento:

1. Clonar de la rama master.
1. Crear una rama para una característica propia.
1. Desarrollar la característica.
1. Enviar a través de Merge Request a la rama de Pruebas.
1. Una vez el equipo de pruebas valide el resultado de la característica éste 
la enviará a través de Merge request a Master.


--

## Normas de la Comunidad

Esta es una iniciativa de las personas todo aporte es valioso por más pequeño 
que sea y por lo tanto el respeto es fundamental

* El trato entre todos los integrantes debe ser cordial.
* No se permite trato ofensivo entre los participantes ni en sus productos generados.
